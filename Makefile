# MODULE_NAME, MAJOR, MINOR and PATCH must be set for each module
# OBJS must be set for each module

SHELL := /bin/bash

CC := gcc
FC := gfortran
RM := @rm -rf -v
AR := ar -q
LN := ln -s
CP := cp
MKDIR := mkdir -p
TOUCH := touch
PRINT := @echo

TESTRUNNER := cgreen-runner
TESTLIB := -lcgreen

BINDIR := ./bin
OBJDIR := ./obj
SRCDIR := ./src
LIBDIR := ./lib
TESTDIR := ./tests
INCLUDEDIR := ./include
INTERFACEDIR := ./interfaces


_OBJS := $(foreach obj, $(OBJS), $(OBJDIR)/$(obj))
_FORTRAN_OBJS := $(foreach obj, $(FORTRAN_OBJS), $(OBJDIR)/$(obj))
_SHARED_OBJS := $(foreach obj, $(_OBJS), $(obj:.o=_shared.o))
_SHARED_FORTRAN_OBJS += $(foreach obj, $(_FORTRAN_OBJS), $(obj:.o=_shared.o))
_TEST_OBJS := $(foreach obj, $(TEST_OBJS), $(OBJDIR)/$(obj))

LIBS += $(foreach lib, $(paCages), -L $(LIBDIR)/$(lib)/$(LIBDIR) -l$(lib))

INCLUDES += -I $(INCLUDEDIR)
INCLUDES += $(foreach lib, $(paCages), -I $(LIBDIR)/$(lib)/$(INCLUDEDIR))


CFLAGS := -Wall -std=c11 $(LIBS) $(INCLUDES)
FFLAGS := -Wall -std=f2003 $(LIBS) $(INCLUDES)


STATIC_LIB_FILE := $(LIBDIR)/lib$(MODULE_NAME).a
STATIC_FLIB_FILE := $(LIBDIR)/lib$(MODULE_NAME)_f.a
SHARED_LIB_FILE := $(LIBDIR)/lib$(MODULE_NAME).so
TEST_FILE := $(TESTDIR)/$(MODULE_NAME)_tests.so


VPATH := $(VPATH):$(SRCDIR):$(TESTDIR):$(INTERFACEDIR)

.PHONY: compile
compile: | $(paCages) $(_OBJS) $(_FORTRAN_OBJS)
ifeq ($(findstring main,$(OBJS)),main)
	$(MAKE) $(BINDIR)/$(MODULE_NAME).$(MAJOR).$(MINOR)
endif


$(BINDIR)/$(MODULE_NAME).$(MAJOR).$(MINOR): $(paCages) $(_OBJS) $(_FORTRAN_OBJS)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: test
test: | $(paCages) $(TEST_FILE)
	$(TESTRUNNER) $(TEST_FILE)

$(TEST_FILE): $(_TEST_OBJS) $(_SHARED_OBJS)
	$(CC) -shared -fPIC -o $@ $^ $(CFLAGS) $(TESTLIB)


.PHONY: libstatic
libstatic: cp_headers $(STATIC_LIB_FILE) $(STATIC_FLIB_FILE)

$(STATIC_LIB_FILE): | $(paCages) $(_OBJS)
	$(AR) $@ $(_OBJS)

$(STATIC_FLIB_FILE): | $(paCages) $(_FORTRAN_OBJS)
	$(AR) $@ $(_FORTRAN_OBJS)


.PHONY: libshared
libshared: cp_headers $(SHARED_LIB_FILE)

$(SHARED_LIB_FILE): | $(paCages) $(_SHARED_OBJS) $(_SHARED_FORTRAN_OBJS)
	$(CC) -shared -Wl,-soname,$(SHARED_LIB_FILE).$(MAJOR) \
	-o $(SHARED_LIB_FILE).$(MAJOR).$(MINOR) $(_SHARED_OBJS)


.PHONY: cp_headers
HEADER_FILES := `find ./$(SRCDIR)/*.h  -printf "%f\n"`
HEADER_DIR := $(INCLUDEDIR)/$(MODULE_NAME)
HEADER_MAIN := $(INCLUDEDIR)/$(MODULE_NAME).h
cp_headers:
	$(MKDIR) $(HEADER_DIR)
	$(TOUCH) $(HEADER_MAIN)
	$(CP) $(SRCDIR)/*.h $(HEADER_DIR)
	@for h in $(HEADER_FILES); do \
		printf "#include <%s/%s>\n" $(MODULE_NAME) $$h >> $(HEADER_MAIN); \
	done;


$(_OBJS): $(OBJDIR)/%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

$(_FORTRAN_OBJS): $(OBJDIR)/%.o: %.f90
	$(FC) -c $(FFLAGS) -o $@ $< -J $(INCLUDEDIR)

$(_SHARED_OBJS): $(OBJDIR)/%_shared.o: %.c
	$(CC) -c -fPIC $(CFLAGS) -o $@ $<

$(_SHARED_FORTRAN_OBJS): $(OBJDIR)/%_shared.o: %.f90
	$(FC) -c -fPIC $(FFLAGS) -o $@ $< -J $(INCLUDEDIR)

$(_TEST_OBJS): $(OBJDIR)/%.o: %.c
	$(CC) -c -fPIC $(CFLAGS) -o $@ $<


.PHONY: $(paCages)
$(paCages):
	@for lib in $(paCages); do \
		make libstatic -C $(LIBDIR)/$$lib; \
	done;


LOCAL_DIR := ${HOME}/.local
.PHONY: install
install:


.PHONY: uninstall
uninstall:


.PHONY: watch
FILES := `find . -regextype sed -regex ".*[?\.git][a-zA-Z0-9].[f|f90|c]"`
watch: compile
	@printf "Watching following file(s)...\n$(FILES)\n"
	@[ command -v inotifywait >/dev/null 2>&1 ] && exit || true;
	@inotifywait -q -m -e modify $(FILES) | \
	while read -r filename event; do make; done;


.PHONY: clean
clean:
	$(RM) $(_OBJS) $(_SHARED_OBJS) $(_TEST_OBJS) $(TEST_FILE)
	$(RM) $(_FORTRAN_OBJS) $(_SHARED_FORTRAN_OBJS)
	$(RM) $(STATIC_LIB_FILE) $(STATIC_FLIB_FILE)
	$(RM) $(SHARED_LIB_FILE).$(MAJOR).$(MINOR)
	$(RM) $(INCLUDEDIR)/$(MODULE_NAME).h
	$(RM) $(INCLUDEDIR)/$(MODULE_NAME)
	$(RM) $(INCLUDEDIR)/*.mod
	$(RM) $(BINDIR)/$(MODULE_NAME).$(MAJOR).$(MINOR)
	@for lib in $(paCages); do \
		make clean -C $(LIBDIR)/$$lib; \
	done;
